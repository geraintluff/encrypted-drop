# Encrypted Drop

31C3 project - single (small) HTML file for uploading encrypted data to DropBox.

[Example here](https://gist.github.com/geraintluff/818eb8501fc95ce57b36) (including private key to test decryption afterwards).

## Generating a "drop page"

If you open `create.html` and enter a DropBox access token and a PGP public key, then it generates an HTML file for you (which it saves as a data URL).

## Using the "drop page"

First, the drop page loads the libraries/resources it needs (from the DropBox account itself).  Before use, the integrity is checked using SHA-256 and known-good values.  This is based on the "bare-bones setup" from my ["Tamper-Proof Web-Apps" presentation](http://geraintluff.bitbucket.org/caution-presentation/).

Once loaded, the user can drag+drop files onto the page.  These files will be PGP-encrypted (using [openpgpjs](http://openpgpjs.org/)) and uploaded to DropBox.