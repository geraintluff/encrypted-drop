var PUBLIC_KEY = null;

// HTTP GET, enhanced with access token
function request(method, url, data, callback) {
	if (url.indexOf('?') === -1) {
		url += '?';
	} else {
		url += '&';
	}
	url += 'access_token=' + ACCESS_TOKEN;
	
	var request = new XMLHttpRequest;
	request.open(method, url, true);
	request.onreadystatechange = function () {
		if (request.readyState !== 4) return;
		if (request.status < 200 || request.status > 299) {
			return callback(new Error('HTTP Status: ' + request.status));
		}
		callback(null, request.responseText);
	};
	window.r = request;
	request.send(data);
	return request;
}

function getJson(url, callback) {
	request('GET', url, null, function (error, data) {
		if (error) return callback(error);
		try {
			data = JSON.parse(data);
		} catch (e) {
			return callback(e, data);
		}
		callback(null, data);
	});
}

function getDir(path, callback) {
	var url = 'https://api.dropbox.com/1/metadata/auto' + path;
	getJson(url, function (error, result) {
		if (error) return callback(error);
		callback(null, result.contents);
	});
}

function getFile(path, callback) {
	var url = 'https://api-content.dropbox.com/1/files/auto' + path;
	request('GET', url, null, function (error, data) {
		if (error) return callback(error);
		callback(null, data);
	});
}

function getFileWithHash(path, hash, callback) {
	getFile(path, function (error, data) {
		if (sha256(data) !== hash) {
			return callback(new Error('Hash mismatch: ' + path));
		}
		callback(null, data);
	});
}

function putFile(path, data, callback) {
	var url = 'https://api-content.dropbox.com/1/files_put/auto' + path + '?overwrite=1';
	return request('PUT', url, data, function (error, response) {
		if (error) return callback(error);
		try {
			response = JSON.parse(response);
		} catch (e) {
			return callback(e, response);
		}
		callback(null, response);
	});
}

/** UI **/

init(function (error) {
	if (error) {
		alert('INITIAL ERROR\n' + error.message);
		throw error;
	}
});

function init(callback) {
	document.body.innerHTML = 'loading public key';
	getFileWithHash('/pgp-key.pub', PK_HASH, function (error, keyText) {
		if (error) return callback(error);
		document.body.innerHTML = 'loading openpgp.js';
		getFileWithHash('/openpgp.js', OPENPGP_HASH, function (error, code) {
			if (error) return callback(error);
			Function(code)();
			PUBLIC_KEY = openpgp.key.readArmored(keyText);
			document.body.innerHTML = 'loading stylesheet';
			getFileWithHash('/stylesheet.css', STYLE_HASH, function (error, css) {
				if (error) return callback(error);
				var element = document.createElement('style');
				element.appendChild(document.createTextNode(css));
				document.head.appendChild(element);

				createUI();
			});
		});
	});
}

function createUI() {
	document.title = "Upload files";
	document.body.innerHTML = '';

	function listFiles(path, dirList) {
		dirList = dirList || document.createElement('ul');
		dirList.className = 'dir-list';
		dirList.innerHTML = 'loading file list...';
		
		getDir('/', function (error, files) {
			dirList.innerHTML = '';
			files.sort(function (a, b) {
				a = new Date(a.modified);
				b = new Date(b.modified);
				return b.getTime() - a.getTime();
			});
			if (error) {
				dirList.appendChild(document.createTextNode(error.message));
				throw error;
			}
			files.forEach(function (file) {
				var entry = document.createElement('li');

				var name = document.createElement('a');
				name.className = 'file-name';
				name.href = '#';
				name.appendChild(document.createTextNode(file.path.replace(/.*\//, '')));
				entry.appendChild(name);

				var url = 'https://api-content.dropbox.com/1/files/auto' + file.path + '?access_token=' + ACCESS_TOKEN;
				name.onclick = function (event) {
					window.open(url);
					event.preventDefault();
					return false;
				};

				var size = document.createElement('span');
				size.className = 'file-size';
				size.appendChild(document.createTextNode(file.bytes + " bytes"));
				entry.appendChild(size);

				var modified = document.createElement('span');
				modified.className = 'file-modified';
				modified.appendChild(document.createTextNode(file.modified));
				entry.appendChild(modified);

				dirList.appendChild(entry);
			});
		});
		return dirList;
	}
	var listing = listFiles('/');
	document.body.appendChild(listing);
	function updateListing() {
		listFiles('/', listing);
	}

	// Actions go at the top
	var actionBlock = document.createElement('div');
	document.body.insertBefore(actionBlock, document.body.childNodes[0]);

	// "Drag+drop to upload
	var instructions = document.createElement('div');
	instructions.className = 'instructions';
	instructions.innerHTML = 'Drag files onto this page to upload';
	document.body.insertBefore(instructions, document.body.childNodes[0]);

	document.body.addEventListener('dragover', function (event) {
		event.preventDefault();
	}, false);
	document.body.addEventListener('drop', getFiles, false);
	function getFiles(event) {
		event.preventDefault();
		var files = [];
		for (var i = 0; i < event.dataTransfer.files.length; i++) {
			files.push(event.dataTransfer.files[i]);
		}
		for (var i = 0; i < files.length; i++) {
			var file = files[0];
			window.file = file;

			var path = '/upload' + Math.random().toString().substring(2) + '.pgp';
			var loading = document.createElement('div');
			loading.className = 'loading';
			var status = document.createElement('span');
			status.className = 'status';
			loading.appendChild(status);
			loading.appendChild(document.createTextNode(' ' + file.name));
			actionBlock.appendChild(loading);

			// Read file as binary string
			status.innerHTML = 'reading';
			var reader = new FileReader();
			reader.addEventListener("loadend", function() {
				// Encrypt with PGP
				status.innerHTML = 'encrypting';
				var literal = new openpgp.packet.Literal();
				literal.setFilename(file.name);
				literal.setBytes(reader.result, 'binary');
				var packetList = new openpgp.packet.List();
				packetList.push(literal);
				var message = new openpgp.message.Message(packetList);
				var m = message.encrypt(PUBLIC_KEY.keys);
				var armoredText = m.armor();
				
				// Upload
				status.innerHTML = 'uploading';
				var req = putFile(path, armoredText, function (error) {
					if (error) {
						status.innerHTML = 'failed';
						alert(error.message);
						throw error;
					} else {
						loading.parentNode.removeChild(loading);
						updateListing();
					}
				});
				window.req = req;
				if (req.upload) {
					req.upload.addEventListener('progress', function (event) {
						console.log(event);
						if (event.lengthComputable) {
							status.innerHTML = 'uploading (' + event.loaded + '/' + event.total + ')';
						}
					});
				}
			});
			reader.readAsBinaryString(file);
		}
	}
}
